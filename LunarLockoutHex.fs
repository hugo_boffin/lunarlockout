﻿module LunarLockoutHex

type TokenWithID = { Name : string; ID : int; }

type Token =
    | Player of TokenWithID
    | Robot of TokenWithID
    | Destination of TokenWithID
    | EmptySpace
    | Void // For dead hexagons .. not very nice

type Direction = N | NE | SE | S | SW | NW

type Board = { Fields : Token[,]; }

let simpleBoardLayout =
    { Fields = Array2D.init 10 5 (fun x y -> (match (x,y) with
                                              | (0, 0) -> Robot {Name=""; ID=0;}
                                              | (1, 0) -> Destination {Name=""; ID=0;}
                                              | (9, 4) -> Player {Name="Pingu"; ID=0;}
                                              | _ -> EmptySpace
                                 ) ); }

let printBoard board =
    for y in 0 .. (Array2D.length2 board.Fields)*2-1 do
        for x in 0 .. (Array2D.length1 board.Fields)-1 do
            match x % 2 = y % 2 with
            | true  -> //printf "%2i " ((y/2)*5+x)
                       match board.Fields.[x, y/2] with
                       | Player p -> printf "P"
                       | Robot r -> printf "R"
                       | Destination d -> printf "D"
                       | EmptySpace -> printf "."
                       | Void -> printf " "
                       printf " "
            | false -> printf "  "
        printfn ""
//
