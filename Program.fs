﻿module Program

open System
open LunarLockout
//open LunarLockoutHex

// GameFinished?
// GameStranded?
// CanBePushed?
// Stackoverflow question : if discriminated union
// TODO: Lunar Lockout Varianten über Funktionen höherer Ordnung
//type Navigation = 
//    | FourAxis
//    | SixAxis
// Board als discriminated union
// MapShape, Hexagon, Diamond(+abgeschnitten), Rectangular
//    { Fields = Array.init (3+4+5+4+3) (fun i -> Robot {Name=""; ID=i;Width=10;Height=5;} )  }
// Array.mapi (fun x y -> 0) [|1..(n*2)|] ... besser


[<EntryPoint>]
let main argv = 
    //printBoard simpleBoardLayout
    let board = simpleBoardLayout
    printBoard board
    let board = push board (4,2) Left
    printBoard board
    Console.ReadKey() |> ignore
    0