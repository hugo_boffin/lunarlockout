﻿module LunarLockout

// Classic Lunar Lockout 5x5 (nxn)

type TokenWithID = { 
    Name : string; 
    ID : int;
}

type Token =
    | Player of TokenWithID
    | Robot of TokenWithID
    | Destination of TokenWithID
    | EmptySpace

type Direction = 
    | Up
    | Down
    | Left
    | Right

type Board = { Fields : Token[,]; } // geht das auch ohne record drum herum?

let simpleBoardLayout =
    { Fields = Array2D.init 5 5 (fun x y -> (match (x,y) with
                                             | (0,2) -> Robot {Name=""; ID=0;}
                                             | (1,2) -> Destination {Name=""; ID=0;}
                                             | (4,2) -> Player {Name="Pingu"; ID=0;}
                                             | _ -> EmptySpace
                                ) ) }

let printBoard board =
    for row in 0 .. (Array2D.length1 board.Fields) - 1 do   // Finster, aber gerade keine bessere fkt. Loesung
        for col in 0 .. (Array2D.length2 board.Fields) - 1 do // ^^ dito
            match board.Fields.[col,row] with
            | Player p -> printf "P"
            | Robot r -> printf "R"
            | Destination d -> printf "D"
            | EmptySpace -> printf "."
        printfn ""

let flatten2dArray arr =
  seq {for i in 0..Array2D.length1 arr - 1 do
         for j in 0..Array2D.length2 arr - 1 do yield arr.[i,j]}

let printPlayerCount board =
    printfn "%i player" <| (fun state element -> state+1) 0 (flatten2dArray board.Fields)

let getNewPosition (x,y) direction =
    match direction with
    | Up -> (x,y-1)
    | Down -> (x,y+1)
    | Left -> (x-1,y)
    | Right -> (x+1,y)

let rec findDestination board (x, y) direction =
    let (nx,ny) = getNewPosition (x, y) direction
    if (nx >= 0 && nx < Array2D.length1 board.Fields &&
        ny >= 0 && ny < Array2D.length2 board.Fields &&
        match board.Fields.[nx,ny] with | EmptySpace -> true | _ -> false)
    then findDestination board (nx,ny) direction
    else (nx,ny)

let push board (x, y) direction =
    match board.Fields.[x,y] with
    | Player p -> let (nx,ny) = findDestination board (x,y) direction
                  printfn "Push %A -> %A" (x,y) (nx,ny)
                  { Fields = Array2D.mapi ( fun ax ay old -> match (ax,ay) with 
                                                             | (ax,ay) when (ax,ay) = (x,y) -> EmptySpace
                                                             | (ax,ay) when (ax,ay) = (nx,ny) -> board.Fields.[x,y]
                                                             | _ -> old
                                          ) board.Fields }
    | _ -> board